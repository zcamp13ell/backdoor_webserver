#include "http_parser.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
char *lines[MAX_LINES];

char *read_lines(char *buffer)
{
    char *ptr;
    int i = 1;
    ptr = buffer;
    lines[0] = ptr; // add first line to lines

    while (ptr != NULL && i < MAX_LINES)
    {
        ptr = strchr(ptr, (int)"\n"); // Find first occurance of LF
        *ptr = (char)"\0";            // Replace with NULL
        lines[i] = ++ptr;             // Add pointer to lines and increment
        i++;
    }

    return (char *)lines;
}

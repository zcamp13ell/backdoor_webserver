CXX 	   := clang

BIN		   := BIN
EXECUTABLE := main

all: $(BIN)/$(EXECUTABLE)

run: clean all
	 
	 ./$(BIN)/$(EXECUTABLE)
$(BIN)/$(EXECUTABLE): *.c
	$(CXX) -g $^ -o $@

clean:
	-rm -rf $(BIN)/*
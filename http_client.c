#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>

#define PORT 2543
#define MESSAGE "Test Message!!"
#define SERVERHOST "127.0.0.1"

int client_connect(char *address, char *port)
{
    int sock_fd;
    struct addrinfo hints, *res;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    getaddrinfo(address, port, &hints, &res);

    if ((sock_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) < 0)
    {
        perror("Client Socket");
        exit(EXIT_FAILURE);
    }

    if (connect(sock_fd, res->ai_addr, res->ai_addrlen) < 0)
    {
        perror("Client Connets");
        exit(EXIT_FAILURE);
    }

    return sock_fd;
}
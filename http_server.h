#ifndef __HTTP_SERVER_H
#define __HTTP_SERVER_H
#define DEFAULT_PORT "5555" // If no port specified
#define MAX_BUFFER 2048     // Static buffer size
#define BACKLOG 5           // Max number of backlog
#define MAX_CONNECTIONS 5
#define MIN_BUFFER 200
#define HTTP_404 "HTTP/1.1 404 Not Found"
#define HTTP_200 "HTTP/1.1 200 OK"

struct server_t
{
    char *rx_buffer;
    char *tx_buffer;
    int server_fd;
    int client_fds[5];
    void (*message_handler)(struct server_t *self);
};
extern struct server_t *SERVER;
int socket_init(char *port);
int read_socket(int file_desc);
char *handle_message(int socket_fd);
void socket_poll(int socket_fd, char *(*callback)(int fd), char *buffer);
char *parse_exec_command(char *buffer, char *res_buffer);
void build_http_response(int http_res, char *buffer);
char get_decoded_char(char *character);
char *call_command(char *command, char *response);
#endif
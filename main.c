#include <stdio.h>
#include "http_server.h"
#include <signal.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

char http_req[] = "GET /exec/ls%20-la%20-f HTTP/1.1\\r\\nHost: localhost:5555\\r\\nConnection: keep-alive\\r\\n";
char shell_command[] = "ls";
int socket_fd;

void sigint_handler(int sig)
{
    close(socket_fd);
    printf("\nClosing Server...\n");
    exit(0);
}

int main(int argc, char *argv[])
{
    char *port;
    char *buffer = (char *)calloc(MIN_BUFFER, sizeof(buffer));
    char *command_buffer = (char *)calloc(MIN_BUFFER, sizeof(command_buffer));

    if (argc < 2)
    {
        port = DEFAULT_PORT;
    }
    else
    {
        port = argv[1];
    }
    signal(SIGINT, sigint_handler);
    signal(SIGSTOP, sigint_handler);

    //parse_exec_command(http_req, buffer);
    call_command(shell_command, command_buffer);
    free(buffer);
    free(command_buffer);
//    socket_fd = socket_init(port);
//    socket_poll(socket_fd, handle_message, buffer);
}
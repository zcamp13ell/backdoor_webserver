#include "http_server.h"
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include "http_parser.h"
/*
 *  Creates and configures socket and returns the socket file descriptor 
 * 
 * char *port: Pointer to port as a string
 * 
 */

struct server_t server;
struct server_t *SERVER = &server;

char http_ok[] = "HTTP/1.1 200 OK\r\n\
\r\n\
Hello World.\r\n ";

char http_template[] = "HTTP/1.1 %s\r\n\
\r\n\
%s\r\n";

int make_socket(char *port)
{

    int sock_fd; // Socket file descriptor
    int status;
    struct addrinfo hints;
    struct addrinfo *servinfo; // will point to results

    memset(&hints, 0, sizeof(hints)); // make sure this is empty
    hints.ai_family = AF_UNSPEC;      // Use either IPV4 or IPV6
    hints.ai_socktype = SOCK_STREAM;  // TCP stream socket
    hints.ai_flags = AI_PASSIVE;      // fill in the ip addr, will use localhost

    /* Check for errors when getting address info */
    if ((status = getaddrinfo(NULL, port, &hints, &servinfo)) != 0)
    {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
    }
    /* Check for errors when creating socket. */
    sock_fd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if (sock_fd < 0)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }
    /* Check for errors when binding socket to address and port. */
    if (bind(sock_fd, servinfo->ai_addr, servinfo->ai_addrlen) < 0)
    {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    // free linked list
    freeaddrinfo(servinfo);

    /* return configured socked file descriptor. */
    return sock_fd;
}

char *dynamic_read_socket(int fd)
{
    char *buffer;
    int nbytes;
    char *temp_buff;
    int buffer_length = MIN_BUFFER;
    buffer = (char *)calloc(buffer_length, (sizeof(buffer)));

    if (!buffer)
    {
        perror("Memory Allocation");
        exit(EXIT_FAILURE);
    }

    nbytes = recv(fd, buffer, buffer_length, 0);

    while (1)
    {
        buffer_length += MIN_BUFFER;
        temp_buff = realloc(buffer, buffer_length);

        if (!temp_buff)
        {
            perror("Buffer Realloc");
            exit(EXIT_FAILURE);
        }

        buffer = temp_buff;
        nbytes = recv(fd, buffer + buffer_length - MIN_BUFFER, MIN_BUFFER, 0);

        if (nbytes < MIN_BUFFER)
        {
            break;
        }
    }
    printf("%s", buffer);
    return buffer;
}

int read_socket(int file_desc)
{
    char buffer[MAX_BUFFER];
    int nbytes;
    nbytes = recv(file_desc, buffer, MAX_BUFFER, 0);
    if (nbytes < 0)
    {
        perror("Read");
        exit(EXIT_FAILURE);
    }
    else if (nbytes == 0)
    {
        return -1;
    }
    else
    {
        fprintf(stderr, "Server got message: `%s\n", buffer);
        return 0;
    }
}
/*
    Initializes a socket at localhost:<port>. Begins listening
*/

int socket_init(char *port)
{
    int sock;

    sock = make_socket(port);

    if (listen(sock, BACKLOG) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    return sock;
}

void socket_poll(int socket_fd, char *(*callback)(int fd), char *buffer)
{
    int client_fd;
    struct sockaddr_storage client_addr;
    socklen_t addr_size;
    char client_ip_name[INET6_ADDRSTRLEN];

    addr_size = sizeof(client_addr);

    while (1)
    {
        client_fd = accept(socket_fd, (struct sockaddr *)&client_addr, &addr_size);
        if (client_fd < 0)
        {
            perror("accept");
            exit(EXIT_FAILURE);
        }
        inet_ntop(AF_INET, &client_addr, client_ip_name, sizeof client_ip_name);
        printf("Server got connection from: %s\n", client_ip_name);
        buffer = callback(client_fd);
        // send(client_fd, buffer);
        close(client_fd);
    }
}

char *handle_message(int socket_fd)
{
    char *ptr;
    char *buffer;
    buffer = dynamic_read_socket(socket_fd);
    int http_res = 404;
    //printf("%s", buffer);
    char *res_buff = (char *)calloc(MIN_BUFFER, sizeof(res_buff));

    parse_exec_command(buffer, res_buff);

    build_http_response(http_res, res_buff);

    if (send(socket_fd, http_template, strlen(http_template), 0) < 0)
    {
        perror("response");
        exit(EXIT_FAILURE);
    }
    //
    return buffer;
}

char *parse_exec_command(char *buffer, char *res_buffer)
{
    char *exec, *ptr, *ptr2, *tmp_buff, *res_ptr;
    int buffer_size = MIN_BUFFER;
    res_ptr = res_buffer;
    char decoded_char;

    ptr = strstr(buffer, "/exec");
    /* If null ptr, print 404 into res_buff */
    if (!ptr)
    {

        return res_ptr;
    }
    ptr2 = strchr(ptr, ' '); // Find space between request and HTTP/1.1

    if (ptr2)
    {
        *ptr2 = NULL;
    }

    ptr2 = strchr(ptr, '%');
    ptr += 6; // Point to beginning of command.

    // Otherwise we need to decode url.
    while (ptr2)
    {
        if ((ptr2 - ptr) > buffer_size + 2) // +2 to account for possible char
        {
            buffer_size += (ptr2 - ptr) + MIN_BUFFER;
            tmp_buff = realloc(res_buffer, buffer_size);

            if (!tmp_buff)
            {
                perror("Realloc res_buffer");
                exit(EXIT_FAILURE);
            }
            res_buffer = tmp_buff; //reassign if alls good
        }

        strncpy(res_ptr, ptr, (ptr2 - ptr));
        res_ptr += (ptr2 - ptr); // move res_ptr to end of string
        ptr2++;                  //increment
        decoded_char = get_decoded_char(ptr2);
        res_ptr = strcpy(res_ptr, &decoded_char);
        res_ptr++;  //increment res_ptr to point to decoded char
        ptr = ptr2 + 2;     // assign ptr to following cmd flag
        ptr2 = strchr(ptr2, '%');   // check for more to decode
    }
    //if no chars to decode, copy remaining command to buffer
    if (strlen(ptr) >= buffer_size)
    {
        tmp_buff = realloc(res_buffer, strlen(ptr)); // Expand to size of ptr.
        if (!tmp_buff)
        {
            perror("realloc res_buffer");
            exit(EXIT_FAILURE);
        }
        res_buffer = tmp_buff;
    }
    strncpy(res_ptr, ptr, strlen(ptr));
    return res_buffer;
}

void build_http_response(int http_res, char *buffer)
{
    char http_code[2];
}

char get_decoded_char(char *character)
{
    if (strcmp(character, "20"))
    {
        return ' ';
    }
    if (strcmp(character, "21"))
    {
        return "!";
    }
    if (strcmp(character, "22"))
    {
        return " ";
    }
    if (strcmp(character, "23"))
    {
        return " ";
    }
    if (strcmp(character, "24"))
    {
        return " ";
    }
    if (strcmp(character, "25"))
    {
        return " ";
    }
    if (strcmp(character, "26"))
    {
        return " ";
    }
    if (strcmp(character, "27"))
    {
        return " ";
    }
    if (strcmp(character, "28"))
    {
        return " ";
    }
    if (strcmp(character, "29"))
    {
        return " ";
    }
    if (strcmp(character, "2A"))
    {
        return " ";
    }
    if (strcmp(character, "2B"))
    {
        return " ";
    }
    if (strcmp(character, "2C"))
    {
        return " ";
    }
    if (strcmp(character, "2D"))
    {
        return " ";
    }
    if (strcmp(character, "2E"))
    {
        return " ";
    }
    if (strcmp(character, "2F"))
    {
        return " ";
    }
    if (strcmp(character, "30"))
    {
        return " ";
    }
    if (strcmp(character, "31"))
    {
        return " ";
    }
    if (strcmp(character, "32"))
    {
        return " ";
    }
    if (strcmp(character, "33"))
    {
        return " ";
    }
}

char *call_command(char *command, char *response)
{
    FILE *fp;
    int status;

    fp = popen(command, "r");
    fread(response, 1, MIN_BUFFER, fp); //read response
    printf("%s", response);
}